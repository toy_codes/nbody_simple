import sys
import os
import nbody.functions as func
from nbody.plot import Plot
from nbody.constants import *
import matplotlib.pyplot as plt
import numpy as np

# def plot_upto_step(xvar, yvar, axis,**kwargs):
#     """Plots the line of the trajectory up to the current time, puts a marker at the current position"""
#     axis.plot(xvar,yvar,**kwargs)
#     axis.plot(xvar[-1],yvar[-1],marker='o',**kwargs)
def plot_upto_step(xvar, yvar, axis=None,zvar=None, **kwargs):
    """Plots the line of the trajectory up to the current time, puts a marker at the current position
    INPUT:
    xvar: x position 1-D array,
    yvar: y position 1-D array,
    zvar: z position 1-D array (optional, only for 3D),
    axis: axis object
"""
    if hasattr(zvar, "__len__"):
        axis.plot(xvar,yvar,zvar,**kwargs)
        axis.scatter(xvar[-1],yvar[-1],zvar[-1],marker='o', alpha=1.,color='k')
    else:
        axis.plot(xvar,yvar,**kwargs)
        axis.plot(xvar[-1],yvar[-1],marker='o', alpha=1.,color='k')

def distance_of_helion_approach(time, pos1, pos2, mode='perihelion'):
    """returns the time and distances at different helion approaches"""
    dist = np.sqrt(np.sum((pos1-pos2)**2,axis=0))
    dist_prev = dist[:-2]
    dist_next = dist[2:]
    dist_mid = dist[1:-1]
    if mode=='perihelion': #perihelion approach
        truth_arr = (dist_mid < dist_prev)*(dist_mid < dist_next)
    elif mode=='epihelion': #epihelion approach
        truth_arr = (dist_mid > dist_prev)*(dist_mid > dist_next)

    time_arr = time[1:-1]
    return time_arr[truth_arr], dist_mid[truth_arr]


def inclination(pos1, pos2, pos_ref, vel1, vel2, vel_ref):
    """return cos(inclination) by using r cross p"""
    print(np.shape(pos1))
    dir1 = np.cross(pos1-pos_ref, vel1-vel_ref)
    print(np.shape(dir1))
    norm1 = np.sqrt(np.sum(dir1**2, axis=1))
    dir2 = np.cross(pos2-pos_ref, vel2-vel_ref)
    norm2 = np.sqrt(np.sum(dir2**2, axis=1))
    return np.sum(dir1*dir2, axis=1)/norm1/norm2


def eccentricity(pos1, pos_ref, vel1, vel_ref, m1, mref):
    """returns eccentricity"""
    ke = 0.5*np.sum((vel1)**2, axis=1)
    pe = -G*mref/np.sqrt(np.sum((pos1-pos_ref)**2,axis=1))
    tote = ke+pe
    print("ke pe shape: {} {}".format(np.shape(ke), np.shape(pe)))
    l = np.cross(pos1-pos_ref, vel1-vel_ref)
    l2 = np.sum(l*l, axis=1)
    print("l2 shape: {}".format(np.shape(l2)))
    mreduced = m1*mref/(m1+mref)
    alpha = -G*mref
    ecc2 = 1+2*tote*l2/alpha**2/mreduced*m1
    return np.sqrt(ecc2)

def COM(pos1, pos2, vel1, vel2, m1, m2):
    """returns COM position and velocity"""
    pos = (m1*pos1 + m2*pos2)/(m1+m2)
    vel = (m1*vel1 + m2*vel2)/(m1+m2)
    return pos, vel

def plot_earth_sun_comet_trajectory(files, movie_loc ="./", movie_name="out.ogg", movie_step=100):
    assert len(files)==3,f"The number of passed files must be 3 (sun, earth, comet), is actually {len(files)}"
    fig = plt.figure()
    ax = fig.add_subplot(111)
    p1 = Plot(ax, xlabel='x (cm)', ylabel='y (cm)')
    p1.plot_trajectory(files, fig, ax)
    xlim = ax.get_xlim()
    ylim = ax.get_ylim()
    plt.close()

    time_s, pos_s, vel_s, mass_s = func.extract_data_from_file(files[0]) #sun
    time_e, pos_e, vel_e, mass_e = func.extract_data_from_file(files[1]) #earth
    time_c, pos_c, vel_c, mass_c = func.extract_data_from_file(files[2]) #comet
    lines = len(time_s)
    count=0
    for jj in range(1,lines,movie_step):
        print("image for line {}/{}".format(jj,lines))
        count +=1
        fig = plt.figure()
        ax=fig.add_subplot(111)
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        plot_upto_step(pos_s[0,:jj], pos_s[1,:jj],ax)
        plot_upto_step(pos_e[0,:jj], pos_e[1,:jj],ax)
        plot_upto_step(pos_c[0,:jj], pos_c[1,:jj],ax)
        fig.savefig('{}trajectory_{:04d}'.format(movie_loc,count))
        plt.close()
    os.system("ffmpeg -framerate 6 -pattern_type glob -i {}'*.png' -pix_fmt yuv420p {}{}".format(movie_loc, movie_loc,movie_name))
    os.system("rm -rf {}*.png".format(movie_loc))
    
def plot_star_cluster_trajectory(files, movie_loc ="./", movie_name="star_cluster.ogg", movie_step=100):
    """Makes a movie out of the star cluster. The corresponding parameter files need to be run first."""
    time = []
    pos = []
    vel = []
    mass = []
    xlim = [-5.,5.] #limit in pc
    ylim = [-5.,5.]

    print("no of files: {}".format(len(files)))
    #extract data and put into list
    for file in files:
        time_s, pos_s, vel_s, mass_s = func.extract_data_from_file(file)
        time.append(time_s)
        pos.append(pos_s)
        vel.append(vel_s)
        mass.append(mass_s)
    lines = len(time[0])
    count=0
    #################movie###################
    for jj in range(1,lines,movie_step):
        print("image for line {}/{}".format(jj,lines))
        count +=1
        fig = plt.figure(figsize=(16,5))
        ax=fig.add_subplot(131)
        ax1=fig.add_subplot(132)
        ax2=fig.add_subplot(133)
        ax.set_xlim(xlim)
        ax.set_ylim(ylim)
        ax1.set_xlim(xlim)
        ax1.set_ylim(ylim)
        ax2.set_xlim(xlim)
        ax2.set_ylim(ylim)
        ax.set_xlabel("x (pc)")
        ax.set_ylabel("y (pc)")
        ax1.set_xlabel("y (pc)")
        ax1.set_ylabel("z (pc)")
        ax2.set_xlabel("x (pc)")
        ax2.set_ylabel("z (pc)")
        ax.set_aspect("equal")
        ax1.set_aspect("equal")
        ax2.set_aspect("equal")


        for ii in range(len(files)):
            plot_upto_step(pos[ii][0,:jj]/pc, pos[ii][1,:jj]/pc,ax,color='b', alpha=0.7)
            plot_upto_step(pos[ii][1,:jj]/pc, pos[ii][2,:jj]/pc,ax1,color='b', alpha=0.7)
            plot_upto_step(pos[ii][0,:jj]/pc, pos[ii][2,:jj]/pc,ax2,color='b', alpha=0.7)

        # plot_upto_step(pos_e[0,:jj], pos_e[1,:jj],ax)
        # plot_upto_step(pos_c[0,:jj], pos_c[1,:jj],ax)
        fig.suptitle('time={:.02f} year'.format(time[0][jj]/year))
        fig.savefig('{}trajectory_{:04d}'.format(movie_loc,count))
        plt.close()
    os.system("ffmpeg -framerate 6 -pattern_type glob -i {}'*.png' -pix_fmt yuv420p {}{}".format(movie_loc, movie_loc, movie_name))
    os.system("rm {}*.png".format(movie_loc))
    # os.system("mpv {}{}.ogg".format(movie_loc,movie_name))

def plot_kozai_oscillation(files, movie_loc ="./", movie_name="kozai.ogg", movie_step=100):
    xlim = [-1.2, 1.2]
    zlim = [-0.5, 0.5]
    time_s, pos_s, vel_s, mass_s = func.extract_data_from_file(files[0])
    time_j, pos_j, vel_j, mass_j = func.extract_data_from_file(files[1])
    time_c, pos_c, vel_c, mass_c = func.extract_data_from_file(files[2])
    tp, dp = distance_of_helion_approach(time_s, pos_s, pos_c)
    te, de = distance_of_helion_approach(time_s, pos_s, pos_c, mode="epihelion")
    if len(te) > len(tp):
        te = te[:-1]
        de = de[:-1]
    elif len(te) < len(tp):
        tp = tp[:-1]
        dp = dp[:-1]
    ecc = (de - dp)/(de+dp) #eccentricity
    t_ecc = 0.5*(te+tp)
    inc = np.arccos(inclination(pos_c.T, pos_j.T, pos_s.T, vel_c.T, vel_j.T, vel_s.T))*180./np.pi
    lines = int(np.shape(time_s)[0]/10)
    print("lines: ", lines)
    count=0
    #################movie###################
    for jj in range(1,lines,movie_step):
        print("image for line {}/{}".format(jj,lines))
        count +=1
        fig = plt.figure(figsize=(12,16))
        ax=fig.add_subplot(211, projection='3d')
        ax2 = fig.add_subplot(212)
        # ax1=fig.add_subplot(132)
        # ax2=fig.add_subplot(133)
        ax.set_xlim(xlim)
        ax.set_ylim(xlim)
        ax.set_zlim(zlim)
        # ax.view_init(elev=30.)
        # ax1.set_xlim(xlim)
        # ax1.set_ylim(ylim)
        # ax2.set_xlim(xlim)
        # ax2.set_ylim(ylim)
        ax.set_xlabel("x (au)")
        ax.set_ylabel("y (au)")
        ax.set_zlabel("z (au)")
        ax2.set_xlabel("time (year)")
        ax2.set_ylabel("inclination (degrees)")
        ax2.set_xlim([0.,time_s[lines-1]/year])
        ax2.set_ylim([np.min(inc), np.max(inc)])
        # ax1.set_xlabel("y (pc)")
        # ax1.set_ylabel("z (pc)")
        # ax2.set_xlabel("x (pc)")
        # ax2.set_ylabel("z (pc)")
        # ax.set_aspect("equal")
        # ax1.set_aspect("equal")
        # ax2.set_aspect("equal")
        # for ii in range(len(files)):
        # 	plot_upto_step(pos[ii][0,:jj]/pc, pos[ii][1,:jj]/pc,ax,color='b', alpha=0.7)

        # print("shape: ", np.shape(pos_s))
        ax2.plot(time_s[:jj]/year, inc[:jj],'b--')
        ax2.plot(time_s[jj]/year, inc[jj],'bo')
        track = 5*movie_step
        if jj <= track:
            plot_upto_step(pos_s[0,:jj]/au, pos_s[1,:jj]/au, pos_s[2,:jj]/au, ax, color='b', alpha=0.7)
            plot_upto_step(pos_j[0,:jj]/au, pos_j[1,:jj]/au, pos_j[2,:jj]/au, ax, color='k', alpha=0.7)
            plot_upto_step(pos_c[0,:jj]/au, pos_c[1,:jj]/au, pos_c[2,:jj]/au, ax, color='g', alpha=0.7)
        else:
            start = jj-track
            plot_upto_step(pos_s[0,start:jj]/au, pos_s[1,start:jj]/au, pos_s[2,start:jj]/au, ax, color='b', alpha=0.7)
            plot_upto_step(pos_j[0,start:jj]/au, pos_j[1,start:jj]/au, pos_j[2,start:jj]/au, ax, color='k', alpha=0.7)
            plot_upto_step(pos_c[0,start:jj]/au, pos_c[1,start:jj]/au, pos_c[2,start:jj]/au, ax, color='g', alpha=0.7)
        # plot_upto_step(pos[ii][1,:jj]/pc, pos[ii][2,:jj]/pc,ax1,color='b', alpha=0.7)
        # plot_upto_step(pos[ii][0,:jj]/pc, pos[ii][2,:jj]/pc,ax2,color='b', alpha=0.7)
        
        # plot_upto_step(pos_e[0,:jj], pos_e[1,:jj],ax)
        # plot_upto_step(pos_c[0,:jj], pos_c[1,:jj],ax)
        fig.suptitle('time={:.02f} year'.format(time_s[jj]/year))
        fig.savefig('{}trajectory_{:04d}'.format(movie_loc,count))
        plt.close()
    os.system("ffmpeg -framerate 6 -pattern_type glob -i {}'*.png' -pix_fmt yuv420p {}{}".format(movie_loc, movie_loc, movie_name))
    os.system("rm {}*.png".format(movie_loc))


    
if __name__=='__main__':
    plot_earth_sun_comet_trajectory(sys.argv[1:])
    # plot_star_cluster_trajectory(sys.argv[1:])
    # plot_kozai_oscillation(sys.argv[1:])
