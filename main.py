#import modules
import sys
import os
# sys.path.append("{}/nbody/".format(os.getcwd()))
import numpy as np
from itertools import product
import matplotlib.pyplot as plt
import pickle

from nbody.constants import *
import nbody.ic as ic
from nbody.particle import Particle
from nbody.functions import *
import nbody.integrator as ig
import nbody.accelaration as ac
import nbody.timestep as ts
from nbody.functions import *
from nbody.parameters import Parameters
print("length: {}".format(len(sys.argv)))
if len(sys.argv) == 1:
        sys.exit("please enter parameter file as argument, i.e. python main.py parameters.txt or similar")
param = Parameters(sys.argv[1:])

##############read in parameters##################
time = float(param.get("start_time_year"))*year
tend = float(param.get("end_time_year"))*year
steps_per_year = float(param.get("steps_per_year"))
# if len(sys.argv) == 3:
#         steps_per_year = float(sys.argv[2]) #dirty hack to run multiple sims quickly
ic_method = param.get("ic")#"UniformStellarCluster"
int_method = param.get("integration_method")#"LFDKD"
acc_method = param.get("accelaration_method")#"BruteForce"
timestep_method = param.get("timestep_method")#"ConstantTimestep"
Npart = int(param.get("Npart"))
write_loc = param.get("write_loc")#'data/Cluster/LFDKD/high_mass/high_n/'
writename = '{}_'.format(param.get("writefile_prefix"))
os.system("mkdir -p {}".format(write_loc))
##################parameter reading completes#############

###########generate objects depending on different options#############
p = [Particle(i) for i in range(Npart)]
#give particles according to the problem
ea_ =getattr(ic, ic_method)
ea = ea_(p, param)#initial condition object   #,radius=0.5*pc,v_1d=2.*1e5,mass=5*msun)
acc_ = getattr(ac, acc_method)
acc = acc_()#accelaration method object
euler_ = getattr(ig, int_method)
euler = euler_(acc) #integration method object, not just Euler
timestep_ = getattr(ts, timestep_method)
timestep = timestep_() #timestep method object
###################complete###################

mass0, pos_old, vel_old = euler.array_from_part(p)
acc_old = acc.accelaration(pos_old, mass0)
##########main time loop#############
#create a counter for every step and every plot step
counter = 0
plot_counter = 0
file = [open('{}{}{:03d}.txt'.format(write_loc,writename,ii), 'w') for ii in range(len(p))]

[initialise_file(file[ii], p[ii], ii) for ii in range(len(file))]

while (time<tend):
        counter +=1
        if counter%1000==0:
                print("step: {}".format(counter))
        [write_to_file(file[ii], p[ii], time) for ii in range(len(file))]
        dt = timestep.compute_timestep(steps_per_year, p)
        #update position and velocity
        pos_new, vel_new, acc_new = euler.update_new_values(mass0, pos_old, vel_old, acc_old, dt)
        euler.part_from_array(p, pos_new, vel_new)
#plotting
        #make the new position and velocity the old
        pos_old = pos_new
        vel_old = vel_new
        acc_old = acc_new
        #advance timestep
        time +=dt
        # break
# # ############end main time loop#############
[f.close() for f in file]
