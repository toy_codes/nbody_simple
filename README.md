# Nbody simple

A simply N-body code in Python with four initial set-ups already included. Includes a brute-force gravity calculation (can relatively easily be expanded to include a gravity tree), as well as a few simple numerical integration schemes. It is possible to also do some very rudimentary form of softening. Adaptive timestep is not included.

## QUICK START
python main.py parameters/kozai.txt
python plot_systems.py data/Kozai/*

## RUN SIMULATION
try "python main.py parameters/kozai.txt". This should generate 3 txt files inside data/Kozai. You can use other parameter files other than kozai.txt

## VIEWING THE RESULTS
try "python plot_systems.py data/Kozai/*". This should create a video. Inside plot_systems.py, you can comment in or out other plotting routines for the other systems

## CHANGING SIM PARAMS
Modify values and strings in the parameter file inside parameters/ or pass a new parameter file

## OUTPUT FILE FORMAT
ndim
particle id
particle mass
time pos_x pos_y pos_z vel_x vel_y vel_z

## EXTRACT FILE INFO
You can use extract_data_from_file function from nbody/functions.py

## PHYSICAL EXPLANATION OF THE PROBLEMS
Earth-Sun system: simple 2 body problem of the earth rotating around the sun.
Earth-Sun-comet system: Addition of a comet whose eccentricity can be changed
Star cluster: A group of stars in a cluster, can be bound or unbound
Kozai Oscillation: A wobbling of the orbit of a planet around the star due to a thirs body. For example, to the sun-jupiter system. This shows up as a change in the inclination. See more at https://en.wikipedia.org/wiki/Kozai_mechanism