import pprint
class Parameters(object):
    """The Parameters class performs the parsing operation on thwe supplied parameter file."""
    param_dict = {
    "ndim":3,
    "writefile":"powerlaw.txt",
    "filename":"SILCC_hdf5_plt_cnt_0438"
    }
    def __init__(self, args): #pass the parameter file
        file1 = args[0]
        f = open(file1)
        for line in f:
            line = line.strip() #strip empty spaces on each side
            if not line: #is an empty line, ignore
                continue
            if line.startswith('#'): #comment, ignore
                continue
            line = line.split("#")[0] #if there is a comment marker, ignore everything after
            paramline = line.split(":")[-1] #split the parameter and take only things after :
            param, value = paramline.split("=") #split the parameter option and its value
            param = param.strip() #strip spaces
            value = value.strip()
            if param in self.param_dict.keys():
                self.param_dict[param]=value
            else:
                # print("WARNING, {} not as default keys, adding".format(param))
                self.param_dict[param]=value
        pp = pprint.PrettyPrinter()
        pp.pprint(self.param_dict)
        self.override_default_params(args[1:])

    def get(self, key):
        """Obtain the param_dict[key] value"""
        if self.param_dict.get(key)==None:
            print("warning, {} not found, returning None".format(key))
        return self.param_dict.get(key)

    def set(self, key, value):
        """Set the param_dict[key] value"""
        self.param_dict[key]=value

    def override_default_params(self,listval):
        for lv in listval:
            if lv.startswith('--'):
                lv = lv.strip('-')
                key, val = lv.split("=")
                if "," in val:
                    val = val.split(",")
                self.set(key,val)
                print(f"OVERRIDE: {key}: {val}")
            else:
                pass
                    
