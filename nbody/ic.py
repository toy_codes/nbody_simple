from nbody.constants import *
import numpy as np
from nbody.particle import Particle
class IC(object):
        """ic option name should be as name of IC class child object
        Example: ic = EarthSunSystem in parameter file"""
        def __init__(self, number):
                super(IC, self).__init__()
                self.N = number


class EarthSunSystem(IC):
        """Sets up an earth-sun system with a 1 Astronomical Unit separation"""
        def __init__(self, p, param):
                # ecc = param.get("eccentricity")
                # print(ecc, "eccentricity")
                super().__init__(len(p))
                self.fill_mass(p)
                # self.Wpos[1,0] = 1*au
        def fill_mass(self, p):
                p[0].mass = 1*msun
                p[1].mass = 1*mearth
                p[0].pos = np.zeros(ndim)
                p[1].pos[0] = 1*au
                p[0].vel = np.zeros(ndim)
                p[1].vel[1] = np.sqrt(G*p[0].mass/au)
                print("vel: ", p[1].vel[1])

class EarthSunMoonSystem(IC):
        """Sets up an earth-sun-moon system with a 1 Astronomical Unit separation between Earth and Sun and the standard distance between Earth and Moon"""
        def __init__(self, p, param):
                super().__init__(len(p))
                self.fill_mass(p)
                print(p[0].vel, p[1].vel, p[2].vel)
                # self.Wpos[1,0] = 1*au
        def fill_mass(self, p):
                p[0].mass = 1*msun
                p[1].mass = 1*mearth
                p[2].mass = 1*mmoon

                p[0].pos = np.zeros(ndim)
                p[1].pos = np.zeros(ndim)
                p[2].pos = np.zeros(ndim)
                p[1].pos[0] = 1*au
                p[2].pos[0] = 1*au + rmoon

                p[0].vel = np.zeros(ndim)
                p[1].vel = np.zeros(ndim)
                p[2].vel = np.zeros(ndim)
                p[1].vel[1] = np.sqrt(G*p[0].mass/au)
                # p[2].vel = p[1].vel
                p[2].vel[1] = np.sqrt(G*p[0].mass/au) + np.sqrt(G*p[1].mass/rmoon)
                print(p[0].vel, p[1].vel, p[2].vel)
                print(p[0].pos, p[1].pos, p[2].pos)

class EarthSunCometSystem(IC):
        """Sets up an earth-sun system with a comet. The comet needs to be given two parameters: eccentricity (should be between 0 (circular orbit) and 1 (unbound) for a recurring comet"""
        def __init__(self, p, param, epsilon=.6):
                epsilon = float(param.get("eccentricity"))
                perihelion = float(param.get("perihelion_in_au"))*au
                assert epsilon>0, f"The eccentricity {epsilon} should be >0"
                assert epsilon<1, f"The eccentricity {epsilon} should be <1 for a recurring comet"
                super().__init__(len(p))
                self.fill_mass(p, epsilon=epsilon, perihelion=perihelion)
                print(p[0].vel, p[1].vel, p[2].vel)
                # self.Wpos[1,0] = 1*au
        def fill_mass(self, p, epsilon=0.1, perihelion=0.586*au):
                p[0].mass = 1*msun
                p[1].mass = 1*mearth
                p[2].mass = 2.2e17

                p[0].pos = np.zeros(ndim)
                p[1].pos = np.zeros(ndim)
                p[2].pos = np.zeros(ndim)
                p[1].pos[0] = 1*au
                p[2].pos[0] = -perihelion
                a_axis = perihelion/(1.-epsilon)
                v_d = np.sqrt(G*p[0].mass*(2./perihelion - 1./a_axis))

                p[0].vel = np.zeros(ndim)
                p[1].vel = np.zeros(ndim)
                p[2].vel = np.zeros(ndim)
                p[1].vel[1] = np.sqrt(G*p[0].mass/au)
                # p[2].vel = p[1].vel
                # ecc_factor = 2*(1.-np.sqrt(1-epsilon*epsilon))/(epsilon*epsilon)
                # print("ecc factor: ", ecc_factor)
                p[2].vel[1] = -v_d
                print(p[0].vel, p[1].vel, p[2].vel)
                print(p[0].pos, p[1].pos, p[2].pos)

class KozaiOscillation(IC):
        """The Kozai mechanism is a 3-body phenomenon. A sun-planet system for example (here assumed to be Jupiter-like), under the influence of a 3rd body (say comet). See en.wikipedia.org/wiki/Kozai_mechanism for example"""
        def __init__(self, p, param, epsilon=.5, comet_mass=2.2e10, inclination=np.pi/3, m1=msun, m2=0.1*msun, distance=1*au):
                epsilon = float(param.get("eccentricity"))
                comet_mass = float(param.get("comet_mass"))
                inclination = float(param.get("inclination_in_degrees"))*np.pi/180.
                m1 = float(param.get("primary_mass_in_msun"))*msun
                m2 = float(param.get("secondary_mass_in_msun"))*msun
                super().__init__(len(p))
                self.fill_mass(p, epsilon=epsilon, comet_mass=comet_mass, inclination=inclination, m1=m1, m2=m2, distance=distance)

        def fill_mass(self, p, epsilon=0.5, comet_mass=mearth, inclination=np.pi/3, m1=msun, m2=0.1*msun, distance=1*au, rel_dist=0.1):
                p[0].mass = m1
                p[1].mass = m2
                p[2].mass = comet_mass

                p[0].pos = np.zeros(ndim)
                p[1].pos = np.zeros(ndim)
                p[2].pos = np.zeros(ndim)
                tot_m = m1+m2
                p[0].vel = np.zeros(ndim)
                p[1].vel = np.zeros(ndim)
                p[2].vel = np.zeros(ndim)
                p[1].pos[0] = distance
                p[0].vel[1] = -np.sqrt(G*m2**2/distance/tot_m)
                p[1].vel[1] = np.sqrt(G*m1**2/distance/tot_m)#/1.01)


                p[2].pos[0] = -rel_dist*np.cos(inclination)*distance
                p[2].pos[2] = rel_dist*np.sin(inclination)*distance

                dist = np.sqrt(np.sum((p[2].pos-p[0].pos)**2))
                print("distance between comet and primary star: {} au".format(dist/au))

                p[2].vel[1] = p[0].vel[1]-np.sqrt(G*p[0].mass/dist*(1+epsilon))
                # p[2].vel[2] = -np.sqrt(G*p[0].mass/dist*(1+epsilon))*np.sin(inclination)
                print("velocity of comet in rest frame {}".format(p[2].vel))

                time_comet = 2*np.pi*dist/(p[2].vel[1]-p[0].vel[1])
                time_jupiter = 2*np.pi*p[1].pos[0]/p[1].vel[1]
                print("time period: jupiter {} comet {}".format(time_jupiter/year, time_comet/year))


class KozaiOscillationTilted(IC):
        """Similar to KozaiOscillation, but for a tilted configuration"""
        def __init__(self, p, param, epsilon=.5, comet_mass=2.2e10, inclination=np.pi/3, m1=msun, m2=0.1*msun, distance=1*au):
                super().__init__(len(p))
                self.fill_mass(p, epsilon=epsilon, comet_mass=comet_mass, inclination=inclination, m1=m1, m2=m2, distance=distance)

        def fill_mass(self, p, epsilon=0.5, comet_mass=mearth, inclination=np.pi/3, m1=msun, m2=0.1*msun, distance=1*au, rel_dist=0.1):
                p[0].mass = m1
                p[1].mass = m2
                p[2].mass = comet_mass

                p[0].pos = np.zeros(ndim)
                p[1].pos = np.zeros(ndim)
                p[2].pos = np.zeros(ndim)
                tot_m = m1+m2
                p[0].vel = np.zeros(ndim)
                p[1].vel = np.zeros(ndim)
                p[2].vel = np.zeros(ndim)
                p[1].pos[0] = distance
                p[0].vel[1] = -np.sqrt(G*m2**2/distance/tot_m)
                p[1].vel[1] = np.sqrt(G*m1**2/distance/tot_m)#/1.01)


                p[2].pos[0] = -rel_dist*distance
                # p[2].pos[2] = rel_dist*np.sin(inclination)*distance

                dist = np.sqrt(np.sum((p[2].pos-p[0].pos)**2))
                print("distance between comet and primary star: {} au".format(dist/au))
                veld = np.sqrt(G*p[0].mass/dist*(1+epsilon))
                p[2].vel[1] = p[0].vel[1]-veld*np.cos(inclination)
                p[2].vel[2] = p[0].vel[2]+veld*np.sin(inclination)
                # p[2].vel[2] = -np.sqrt(G*p[0].mass/dist*(1+epsilon))*np.sin(inclination)
                print("velocity of comet in rest frame {}".format(p[2].vel))

                time_comet = 2*np.pi*dist/(p[2].vel[1]-p[0].vel[1])
                time_jupiter = 2*np.pi*p[1].pos[0]/p[1].vel[1]
                print("time period: jupiter {} comet {}".format(time_jupiter/year, time_comet/year))


class UniformStellarCluster(IC):
        """A stellar cluster problem is a classic N-body problem. N number of stars are initially uniformly spherically distributed with some random kinetic and potential energy."""
        def __init__(self, p, param, mass=msun, radius=0.25*pc, v_1d=0.2*1e5):
                radius = float(param.get("cluster_radius_in_pc"))*pc
                v_1d = float(param.get("1d_velocity_dispersion"))
                mass = float(param.get("particle_mass_in_msun"))
                super().__init__(len(p))
                self.fill_mass(p, mass=mass, radius=radius, v_1d=v_1d)

        def fill_mass(self, p, mass=msun, radius=0.25*pc, v_1d=0.2*1e5):
                number =len(p)
                print("number of members in star cluster: {}".format(number))
                # [ for i in range(number)]
                # s_v = v_1d
                # b_v = np.sqrt(3.)*s_v
                state = np.random.RandomState(seed=1011)
                rad = state.uniform(low=0.,high=radius,size=number)
                phi = state.uniform(low=0.,high=2*np.pi,size=number)
                theta = state.uniform(low=0.,high=np.pi,size=number)

                for i in range(number):
                        p[i].mass=mass
                        p[i].pos=np.array([rad[i]*np.sin(theta[i])*np.cos(phi[i])
                                ,rad[i]*np.sin(theta[i])*np.sin(phi[i])
                                ,rad[i]*np.cos(theta[i])])
                        # p[i].pos[2]=rad*np.cos(theta)
                        # p[i].pos[0]=rad*np.sin(theta)*np.cos(phi)
                        # p[i].pos[1]=rad*np.sin(theta)*np.sin(phi)
                        # print(rad/pc, np.sqrt(np.sum(p[i].pos**2))/pc)
                        p[i].vel=state.normal(loc=0.,scale=v_1d,size=3)

                radius = np.array([np.sqrt(np.sum(part.pos**2)) for part in p])
                sigma_1d = np.array([np.sqrt(np.sum(part.vel**2)/3.) for part in p])
                max_rad = np.max(radius)
                avg_rad = np.sum(radius/number)
                avg_sigma = np.sum(sigma_1d/number)
                print("radius: {}, max: {}, sigma: {}".format(avg_rad/pc, max_rad/pc, avg_sigma))
                for part in p:
                        print("particle: {}, pos: {}, vel: {}".format(p.index(part), part.pos, part.vel))
