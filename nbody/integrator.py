import numpy as np
from itertools import product
from nbody.functions import *
from nbody.constants import *

class Integrator(object):
        """General Integrator class, not directly used"""
        def __init__(self, acc):
                super(Integrator, self).__init__()
                self.acc_method = acc


        def return_mass_array(self, part_arr):
                """Given a list of particle class objects, returns their mass
                INPUT: part_arr -- list of lenfth Npart containing Particle class objects
                OUTPUT: array of masses of length Npart"""
                return np.array([p.mass for p in part_arr])

        def return_position_array(self, part_arr):
                """Given a list of particle class objects, returns their positions (2D or 3D)
                INPUT: part_arr -- list of lenfth Npart containing Particle class objects
                OUTPUT: array of shape (ndim, Npart), expressing positions"""
                ra = []
                for dim in range(ndim):
                        ra.append([p.pos[dim] for p in part_arr])
                return np.array(ra)

        def return_velocity_array(self, part_arr):
                """Given a list of particle class objects, returns their velocities (2D or 3D)
                INPUT: part_arr -- list of lenfth Npart containing Particle class objects
                OUTPUT: array of shape (ndim, Npart), expressing positions"""
                ra = []
                for dim in range(ndim):
                        ra.append([p.vel[dim] for p in part_arr])
                return np.array(ra)


        def array_from_part(self, part_list):
                """
                Given a list of particle class objects, returns their mass, position, velocities
                INPUT: part_arr -- list of lenfth Npart containing Particle class objects
                OUTPUT: tuple (mass, pos, vel): mass shape: (Npart), pos and vel shape: (ndim, Npart)
                """
                marr = self.return_mass_array(part_list)
                pos = self.return_position_array(part_list)
                vel = self.return_velocity_array(part_list)
                return marr, pos, vel

        def part_from_array(self, part_list, pos_arr, vel_arr):
                """
                Incorporates the position and velocity information of particles (masses added at particle creation time)
                INPUT:
                part_list -- list of particles of length Npart
                pos_arr -- array of positions of shape (ndim,Npart)
                vel_arr -- array of velocities of shape (ndim,Npart)
                OUTPUT:
                None
                """
                Npart = len(part_list)
                for ii in range(Npart):
                        for dim in range(ndim):
                                part_list[ii].pos[dim] = pos_arr[dim,ii]
                                part_list[ii].vel[dim] = vel_arr[dim,ii]


class Euler(Integrator):
        """Forward Euler integrator (first order) for advancing timestep"""
        def __init__(self, acc):
                super(Euler, self).__init__(acc)
                print("acc method: ", self.acc_method)

        def update_new_values(self, marr, pos, vel, acc, h):
                """Main time advancing function. In Euler, for particle i at timestep n:
                In general, for Euler, W_i,(n+1) = W_i,(n) + g_l(W_(n))*h
                x_i,(n+1) = x_i,(n) + v_i,(n)*dt
                v_i,(n+1) = v_i,(n) + a_i,(n)*dt
                The accelaration is computed as the brute force Nbody gravity here. For a tree calculation,
                the tree must be calculated or called to approximately evaluate the accelaration"""
                # acc_n = self.return_gvel_summed(pos, marr) #array of shape (ndim, N)
                pos_np1 = pos + vel*h
                vel_np1 = vel + acc*h
                acc_np1 = self.acc_method.accelaration(pos_np1, marr)
                return pos_np1, vel_np1, acc_np1


class EulerCromer(Integrator):
        """Forward Euler-Cromer integrator (first order) for advancing timestep"""
        def __init__(self, acc):
                super(EulerCromer, self).__init__(acc)

        def update_new_values(self, marr, pos, vel, acc, h):
                """Main time advancing function. In Euler, for particle i at timestep n:
                In general, for Euler, W_i,(n+1) = W_i,(n) + g_l(W_(n))*h
                x_i,(n+1) = x_i,(n) + v_i,(n)*dt
                v_i,(n+1) = v_i,(n) + a_i,(n)*dt
                The accelaration is computed as the brute force Nbody gravity here. For a tree calculation,
                the tree must be calculated or called to approximately evaluate the accelaration"""
                #Wn+1 = Wn + h*gl(Wn)
                vel_np1 = vel + acc*h
                pos_np1 = pos + vel_np1*h
                acc_np1 = self.acc_method.accelaration(pos_np1, marr)
                return pos_np1, vel_np1, acc_np1


class LFKDK(Integrator):
        """Leap Frog Kick-Drift-Kick integrator (second order) for advancing timestep"""
        def __init__(self, acc):
                super(LFKDK, self).__init__(acc)

        def update_new_values(self, marr, pos, vel, acc_n, h):
                Npart = len(marr)
                vel_np05 = vel + 0.5*h*acc_n
                pos_np1 = pos + vel_np05*h
                acc_np1 = self.acc_method.accelaration(pos_np1, marr)
                vel_np1 = vel_np05 + 0.5*h*acc_np1
                return pos_np1, vel_np1, acc_np1

class LFDKD(Integrator):
        """Leap Frog Drift-Kick-Drift integrator (second order) for advancing timestep"""
        def __init__(self, acc):
                super(LFDKD, self).__init__(acc)

        def update_new_values(self, marr, pos, vel, acc, h):
                Npart = len(marr)
                pos_np05 = pos + 0.5*h*vel
                acc_np05 = self.acc_method.accelaration(pos_np05, marr)
                vel_np1 = vel + acc_np05*h
                pos_np1 = pos + 0.5*h*(vel+vel_np1)
                return pos_np1, vel_np1, acc_np05


                                                
