from nbody.constants import *
import numpy as np

class Accelaration(object):
        """General Accelaration class. Not directly used."""
        def __init__(self):
                super(Accelaration, self).__init__()


class BruteForce(Accelaration):
        """BruteForce pythonic implementation of accelaration. Inherits from Accelaration. An O(N^2) calculation, where the effect of each particle on each other particle is computed. The gravitational accelaration for particle i, g_i is:
        g_i = -G sum_j m_j*r_ij/|r_ij|^3, where j =/= i, and r_ij is the distance vector between i-th and j-th particle
"""
        def __init__(self):
                super(BruteForce, self).__init__()

        def accelaration(self, pos_arr, mass_arr):
                """
                Returns the full accelaration matrix calculated from position and mass vector, to be used in the integrator time update.
                INPUT:
                pos_arr -- position array of shape (ndim,Npart)
                mass_arr -- array of shape (Npart,)
                OUTPUT:
                return_mat -- accelaration matrix of shape (ndim,Npart), where (k,i) is the k-th component of gravitational accelartation felt by the i-th particle
                """
                Npart = len(mass_arr)

                pos_ii = []
                pos_jj = []
                for dim in range(ndim):
                        pos_ii.append(np.repeat(pos_arr[dim],Npart).reshape(Npart,Npart)) #position of particles, shaped into 2D matrix for calculation pos_ii is of shape (ndim, Npart, Npart)
                        pos_jj.append(np.repeat(pos_arr[dim],Npart).reshape(Npart,Npart,order='F'))#transverse of previous operation, i.e. pos_jj[dim].T = pos_ii[dim]

                pos_ii = np.array(pos_ii)
                pos_jj = np.array(pos_jj)
                pos_diff = pos_ii - pos_jj #array of shape (ndim, Npart, Npart), difference matrix
                pos_dist = np.sum((pos_ii-pos_jj)**2, axis=0) #distance squared, summed over dim axis
                pos_dist[pos_dist==0.] = np.inf #setting diagonal terms to infinity for distance, to avoid 0/0 division
                pos_matrix = np.array([pos_diff[dim]/(pos_dist+e_soft**2)**1.5 for dim in range(ndim)]) #matrix[dim][i][j] = (x[dim][i]-x[dim][j])/dist(i,j)**3
                return_mat = -G*np.matmul(pos_matrix, mass_arr) #gl[i] = -G*sum_j(m[j]*(x[dim][i]-x[dim][j])/dist(i,j)**3)
                return return_mat
