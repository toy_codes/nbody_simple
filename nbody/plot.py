import numpy as np 
import matplotlib.pyplot as plt

class Plot(object):
	"""docstring for Plot"""
	def __init__(self, ax, xlabel=None, ylabel=None, xlog='linear', ylog='linear'):
		super(Plot, self).__init__()
		ax.set_xlabel(xlabel)
		ax.set_ylabel(ylabel)
		ax.set_xscale(xlog)
		ax.set_yscale(ylog)



	def plot_trajectory(self, files, fig, ax, label=[]):
		for ii in range(len(files)):
			data = np.loadtxt(files[ii], skiprows=3)
			print("data shape: {}".format(np.shape(data)))
			x = data.T[1]
			y = data.T[2]
			if len(label)==0:
				ax.plot(x,y)
			elif len(label)==len(files):
				ax.plot(x,y, label=label[ii])
			else:
				ax.plot(x,y,label=label)
