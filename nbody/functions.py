import numpy as np
import matplotlib.pyplot as plt
from nbody.constants import *
import numpy as np

def distance(arr1, arr2):
        """For 2 1-D arrays, A and B compute their distance, sqrt(Sum_i (A_i-B_i)^2)"""
        return np.sqrt(np.sum((arr1-arr2)**2))

def distance_nth(arr1, arr2, npow):
        """For 2 1-D arrays, A and B compute their n-th power distance, (Sum_i (A_i-B_i)^2)^{npow/2}"""
        return np.power(np.sum((arr1-arr2)**2), 0.5*npow)

def check_ke(marr, vel_arr):
        """Returns total Kinetic energy of system from mass and velocity arrays
        INPUT:
        marr -- mass array of shape (Npart,)
        vel_arr -- velocity array of shape (ndim,Npart)
        OUTPUT:
        total kinetic energy of the system, single value float 
        """
        v2 = np.sum(vel_arr**2, axis=0)
        return 0.5*np.sum(marr*v2)

def compute_distance(pos1, pos2):
        """Given two position arrays, compute distrance
        INPUT:
        pos1, pos2 -- position array of shape (ndim, Npart)
        OUTPUT:
        dist -- distance array of shape (Npart,)
        """
        dist = np.sqrt(np.sum((pos1-pos2)**2,axis=0))
        return dist

def compute_ke(mass, vel_arr):
        """
        COmputes kinetic energy from mass and velocity arrays
        INPUT:
        mass -- mass array of shape (Npart,)
        vel_arr -- velocity array of shape (ndim,Npart)
        OUTPUT:
        kinetic energy array of shape (Npart,)
        """
        v2 = np.sum(vel_arr**2,axis=0)
        return 0.5*mass*v2

def compute_pe(m1, m2, pos1, pos2):
        """
        For two particle arrays, compute the PE between the i-th element of each array.
        INPUT:
        m1,m2 -- mass array of shape (Npart,) or single value
        pos1,pos2 -- position array of shape (ndim,Npart) or (ndim)
        OUTPUT:
        A potential array of shape (Npart,) or single value. The i-th element of the array computes the potential energy between the i-th elements of arrays 1 and 2.

        """
        dist = np.sqrt(np.sum((pos1-pos2)**2,axis=0))
        return -G*m1*m2/dist

def L2_error(arr1, cval):
        """The L2 error computed between obtained values and correct values"""
        carr = cval*np.ones(np.shape(arr1))
        earr = (arr1-carr)**2
        error = np.sqrt(np.sum(earr))/len(arr1)
        return error

def power_law(x,a,b):
        """Returns ax^b"""
        return a*np.power(x,b)

def power_law_log(x,a,b):
        """Returns ln(a) +bx"""
        return np.log(a) + b*x

def inverse(x,a):
        return a/x
def inverse_square(x,a):
        return a/x/x

##############reading and writing functions#################
def initialise_file(file, p, id):
        """
        Prepares the initial header of the file
        INPUT:
        file -- file object
        p -- particle object
        id -- single number denoting the identification id
        OUTPUT:
        None, but modifies the file
        """
        file.write('{}\n'.format(ndim))
        file.write('{}\n'.format(id))
        file.write('{}\n'.format(p.mass))

def write_to_file(file, p, time):
        """
        Writes data to file, currently set up for only 3 dimensions. The overall writing system works as follows, if there are n particles, then n files are created. Each one contains text in the following form
        3 #no of dims
        125 #id of part
        1e6 #mass of particle in cgs
        time pos[0] pos[1] pos[2] vel[0] vel[1] vel[2]
        
        INPUT:
        file -- file object
        p -- particle object
        time -- float, current time
        OUTPUT:
        Nine, but modifies the file
        """
        file.write('{}'.format(time))
        if ndim ==3:
                file.write('\t{}\t{}\t{}'.format(p.pos[0],p.pos[1],p.pos[2]))
                file.write('\t{}\t{}\t{}'.format(p.vel[0],p.vel[1],p.vel[2]))
                file.write('\n')


def extract_data_from_file(file, skiprows=3):
        """INPUT: filepath
        OUTPUT: 
        time, position, velocity, mass

        time: [nsteps] shaped array
        position: [nsteps,ndim] shaped array
        velocity: [nsteps,ndim] shaped array
        mass: single value of particle mass"""
        wdim, id1, mass = np.loadtxt(file, max_rows=3) #dimension, particle id, mass
        wdim = int(wdim)
        data = np.loadtxt(file, skiprows=skiprows)
        time = data.T[0]
        pos = data.T[1:wdim+1]
        vel = data.T[wdim+1:2*wdim+1]
        mass = 1.*mass
        return time, pos, vel, mass


#######################
def earth_sun_energy(files, fig, ax, label=None):
        '''plots total energy, returns it as well. Used for specific earth-sun routine'''
        time_s, pos_s, vel_s, mass_s = extract_data_from_file(files[0])
        time_e, pos_e, vel_e, mass_e = extract_data_from_file(files[1])

        ke_e = compute_ke(mass_e, vel_e)
        ke_s = compute_ke(mass_s, vel_s)
        pe = compute_pe(mass_s, mass_e, pos_s, pos_e)
        tot_e = ke_e + ke_s+ pe
        ax.plot(time_s/year, tot_e, label=label)
        return tot_e

def plot_circle(radius, fig, ax, npoints=10000, **kwargs):
        '''plot circle about 0 with given radius'''
        theta = np.linspace(0.,2.*np.pi, npoints)
        x = radius*np.cos(theta)
        y = radius*np.sin(theta)
        ax.plot(x,y, **kwargs)
