from nbody.constants import *
import sys

class Timestep(object):
	"""Overall Timestep class, not directly used"""
	def __init__(self):
		super(Timestep, self).__init__()

class ConstantTimestep(Timestep):
	"""ConstantTimestep class, where the timestep is simply divided by how many timesteps each year is divied into. dT = 1 year/Nstep"""
	def __init__(self):
		super(ConstantTimestep, self).__init__()

	def compute_timestep(self, Nsteps=200, particle_list=None):#, pos_arr=None, vel_arr=None, eta=1.):
		return year*1./Nsteps
